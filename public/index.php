<?php
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Результаты сохранены');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  exit();
}
$errors = FALSE;
if (empty($_POST['name'])) {
  print('Заполни имя<br/>');
  $errors = TRUE;
}
if (empty($_POST['email'])) {
  print ('Заполни email <br/>');
  $errors = TRUE;
}
else if (!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)) {
  print ('Email введен некорректно<br/>');
  $errors=true;
}

if (empty($_POST['year'])) {
    print('Заполни год<br>');
    $errors = TRUE;
}
else {
    $year = $_POST['year'];
    if (!(is_numeric($year) && intval($year) >= 1900 && intval($year) < 2020)) {
        print("Укажи корректный год<br>");
        $errors = TRUE;
    }
}
$ability_data = ['god', 'clip', 'fly'];
if (empty($_POST['power'])) {
    print('Выбери способность<br>');
    $errors = TRUE;
}
else {
    $abilities = $_POST['power'];
    foreach ($abilities as $ability) {
        if (!in_array($ability, $ability_data)) {
            print('Недопустимая способность<br>');
            $errors = TRUE;
        }
    }
}
$ability_insert = [];
if (!empty($_POST['power'])) {
    foreach ($ability_data as $ability) {
        $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
    }
}


if($errors){
  exit();
}
$user = 'u35653';
$pass = '4017880';
$db = new PDO('mysql:host=localhost;dbname=u35653', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
    //подготовленный запрос
  $stmt = $db->prepare("INSERT INTO info (name,year,sex,email,bio,limb,ab_god,ab_fly,ab_clip) VALUES (:name,:year,:sex,:email,:bio,:limb,:ab_god,:ab_fly,:ab_clip)");
  $stmt -> bindParam(':name', $_POST['name']);
  $stmt -> bindParam(':year', $_POST['year']);
  $stmt -> bindParam(':sex', $_POST['sex']);
  $stmt -> bindParam(':email', $_POST['email']);
  $stmt -> bindParam(':bio', $_POST['bio']);
  $stmt -> bindParam(':limb', $_POST['limb']);
  $stmt -> bindParam(':ab_god', $ability_insert['god']);
  $stmt -> bindParam(':ab_fly', $ability_insert['fly']);
  $stmt -> bindParam(':ab_clip', $ability_insert['clip']);
  $stmt -> execute();
}
catch(PDOException $e) {
  print('Error : ' . $e->getMessage());
  exit();
}

header('Location: ?save=1'); 

?>
